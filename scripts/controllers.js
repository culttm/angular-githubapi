/**
 * partition description
 * @param  {array} arr  массив данных из скоупа
 * @param  {integer} size делитель
 * @return {array}      часть массива деленного на "size"
 */
function partition(arr, size) {
	var newArr = [];
		for (var i=0; i<arr.length; i+=size) {
			newArr.push(arr.slice(i, i+size));
		}
	return newArr;
};


/**
 * Фун-я пагинирования
 * @param  {Object} $scope      нужный скоуп
 * @param  {integer} itemperpage колличество выводимых элементов
 * @param  {integer} rangesize   диапозон ссылок в пагинаторе
 *
 * TODO Перепилить
 */
function pagination($scope, itemperpage, rangesize ){

	$scope.itemsPerPage = itemperpage;
	$scope.currentPage = 0;

	$scope.range = function() {
		var rangeSize = rangesize;
		var range = [];
		var start;

		start = $scope.currentPage;
		
		// Проверка на ограничение по кол-ву  всех эелементов
		if ( start > $scope.pageCount()-rangeSize ) {
			start = $scope.pageCount()-rangeSize+1;					
		}

		// формируем скоуп для ссылок в пагинаторе
		for (var i=start; i<start+rangeSize; i++) {
			range.push(i);
		}


		return range;
	};

	$scope.prevPage = function() {
		if ($scope.currentPage > 0) {
		  $scope.currentPage--;
		}
	};

	$scope.prevPageDisabled = function() {
		return $scope.currentPage === 0 ? "disabled" : "";
	};

	$scope.pageCount = function() {
		return Math.ceil($scope.rows.length/$scope.itemsPerPage)-1;
	};

	$scope.nextPage = function() {
		if ($scope.currentPage < $scope.pageCount()) {
		  $scope.currentPage++;
		}
	};

	$scope.nextPageDisabled = function() {
		return $scope.currentPage === $scope.pageCount() ? "disabled" : "";
	};

	$scope.setPage = function(n) {
		$scope.currentPage = n;
	};
	
};


/**
 * Контроллер вывода списка юзеров
 * @param {Object} Users  сервис смотрящий на всех юзеров
 */
function UsersListCtrl($scope, $http, Users) {
	$scope.list = [];
	Users.getAllUsers().success(function(data){
		$scope.rows = partition(data, 4);
		pagination($scope, 2, 4)
	});
};	