var app  = angular.module('app.filters', []);

/**
 * Фильтр для смещения вывода элементов
 * @return {array} 
 */
app.filter('offset', function() {
	return function(input, start) {

		if(!input || !input.length){
			return input;
		}else{
		    start = parseInt(start, 10);
	 	    return input.slice(start);
		}
  		 
	};
});
