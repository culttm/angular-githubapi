var app = angular.module('app.services', []);

/**
 * Сервис для получения юзеров
 */
app.service('Users', ['$http', function($http){
	return {
		getAllUsers : function(){
			return $http.get('https://api.github.com/users')
		},		
		getSingleUser : function(name){
			return $http.get('https://api.github.com/users/'+name)
		}

	};
}]);